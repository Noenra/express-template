import { Application } from "express";
import HomeController from "./controllers/HomeController";
import SkillController from "./controllers/SkillController";
import CriteriaController from "./controllers/CriteriaController";
import SignController from "./controllers/SignController";
import LogController from "./controllers/LogController";
import FormacceptController from "./controllers/FormacceptController";

export default function route(app: Application)
{
    /** Static pages **/
    app.get('/', (req, res) =>
    {
        HomeController.home(req, res);
    });

    app.get('/skills', (req, res) =>
    {
        SkillController.skills(req, res);
    });

    app.get('/skills/:id', (req, res) =>
    {
        CriteriaController.criterias(req, res);
    });

    app.post('/skills/:id', (req, res) =>
    {
        CriteriaController.reqcriterias(req, res);
    });


    app.post('/signin', (req, res) =>
    {
        SignController.sign(req, res);
    });

    app.post('/login', (req, res) =>
    {
        LogController.log(req, res);
    });

    app.get('/accept', (req, res) => 
    {
        FormacceptController.formaccept(req, res);
    });
}
