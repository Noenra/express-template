import { Request, Response } from "express-serve-static-core";
import {db} from "../server"

export default class HomeController
{
    static home(req: Request, res: Response): void
    {
        const skills =  db.prepare("SELECT * FROM skills WHERE id<5").all();
        res.render('pages/home', {
            title: 'Accueil',
            tiskill: skills,
        });
    }
}