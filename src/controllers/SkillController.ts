import { Request, Response } from "express-serve-static-core";
import {db} from "../server"

export default class SkillController
{
    static skills(req: Request, res: Response): void
    {
        const skills = db.prepare("SELECT * FROM skills").all();
        res.render('pages/skills', {
            title: "Compétences",
            tabskills: skills,
        });
    }
}