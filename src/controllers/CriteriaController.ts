import { Request, Response } from "express-serve-static-core";
import {db} from "../server"

export default class CriteriaController
{
    static reqcriterias(req: Request, res: Response): void
    {
        const checked = req.body
        console.log(checked);
        /*for(let elem in checked){
            console.log(elem)
        }*/
        

    }
    static criterias(req: Request, res: Response): void
    {
        const criterias = db.prepare("SELECT * FROM criterias WHERE skills_id=?").all(req.params.id);
        const skill = db.prepare("SELECT * FROM skills WHERE id = ?").get(req.params.id);

        res.render('pages/criteria', {
            title: "Critères",
            scriteria: criterias,
            contentskill: skill,
        });
    }
}