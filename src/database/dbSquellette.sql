-- Creator:       MySQL Workbench 6.3.8/ExportSQLite Plugin 0.1.0
-- Author:        Dylan Benchalal--Galopin
-- Caption:       New Model
-- Project:       Name of the project
-- Changed:       2021-12-13 13:54
-- Created:       2021-12-13 13:38
PRAGMA foreign_keys = OFF;

BEGIN;
CREATE TABLE "skills"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "title" VARCHAR(45) NOT NULL,
  "content" VARCHAR(45) NOT NULL
);
CREATE TABLE "users"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "email" VARCHAR(45) NOT NULL,
  "username" VARCHAR(45) NOT NULL,
  "password" VARCHAR(45) NOT NULL,
  "created_at" DATETIME DEFAULT TIMESTAMP,
  "modified_at" DATETIME,
  "deleted_att" DATETIME
);
CREATE TABLE "criterias"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "content" VARCHAR(45) NOT NULL,
  "skills_id" INTEGER NOT NULL,
  CONSTRAINT "fk_criterias_skills1"
    FOREIGN KEY("skills_id")
    REFERENCES "skills"("id")
);
CREATE INDEX "criterias.fk_criterias_skills1_idx" ON "criterias" ("skills_id");
CREATE TABLE "users_has_criterias"(
  "id" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
  "users_id" INTEGER NOT NULL,
  "criterias_id" INTEGER NOT NULL,
  CONSTRAINT "fk_users_has_criterias_users1"
    FOREIGN KEY("users_id")
    REFERENCES "users"("id"),
  CONSTRAINT "fk_users_has_criterias_criterias1"
    FOREIGN KEY("criterias_id")
    REFERENCES "criterias"("id")
);
CREATE INDEX "users_has_criterias.fk_users_has_criterias_criterias1_idx" ON "users_has_criterias" ("criterias_id");
CREATE INDEX "users_has_criterias.fk_users_has_criterias_users1_idx" ON "users_has_criterias" ("users_id");
COMMIT;
